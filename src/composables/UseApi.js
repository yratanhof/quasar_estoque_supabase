import useSupabase from 'boot/supabase'
import useAuthUser from 'src/composables/UseAuthUser'

export default function useApi () {
  const supabase = useSupabase()
  const { user } = useAuthUser()

  const list = async (table) => {
    const { data, error } = await supabase
      .from(table)
      .select( '*')
    if (error) throw error
    return data
  }

  const getById = async (id) => {
    const { data, error } = await supabase
      .from(table)
      .insert()
      .eq('id', id)
    if (error) throw error
    return data[0]
  }

  const post = async (table, data) => {
    const { data, error } = await supabase
      .from(table)
      .insert([
        {
          ...data,
          user_id: user.value.id
        }
      ])

    if (error) throw error
    return data[0]
  }

  const update = async (table, data) => {
    const { data, error } = await supabase
      .from(table)
      .update([
        {
          ...data,
        }
      ])
      .match({
        id: id
      })

    if (error) throw error
    return data[0]
  }

  const destroy = async (table, id) => {
    const { data, error } = await supabase
      .from(table)
      .delete()
      .match({
        id: data.id
      })
  }

  return {
    list,
    getById,
    post,
    update,
    destroy
  }
}
